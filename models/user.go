package models

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Name           string
	Email          string `gorm:"unique_index"`
	hashedPassword []byte
}

func NewUser(name string, email string, password string) (user *User, err error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	user = &User{
		Name:           name,
		Email:          email,
		hashedPassword: hashedPassword,
	}
	return
}

func (user *User) CheckPassword(password string) bool {
	return nil == bcrypt.CompareHashAndPassword(user.hashedPassword, []byte(password))
}
