package models

import (
	"github.com/jinzhu/gorm"
)

type MenuItem struct {
	gorm.Model
	Position uint8
	Menu     Menu
	Page     Page
}
