package models

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestNewUser(t *testing.T) {
	Convey("User password hash generation and check", t, func() {

		user, err := NewUser("John", "john@test.com", "password")

		So(err, ShouldEqual, nil)
		So(user.Name, ShouldEqual, "John")
		So(user.Email, ShouldEqual, "john@test.com")
		So(user.CheckPassword("password"), ShouldEqual, true)
		So(user.CheckPassword("passwordgerkzgzelgzeg"), ShouldEqual, false)

	})
}
