package models

import (
	"github.com/jinzhu/gorm"
)

type Page struct {
	gorm.Model
	Title   string
	Content string
	User    *User
}
