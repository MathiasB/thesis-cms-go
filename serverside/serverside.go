package main

import (
	"html/template"
	"io"
	"net/http"
	"os"
	"strconv"

	"github.com/DenBeke/cms/controllers"
	"github.com/DenBeke/cms/db"

	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
)

func seed() {
	_, err := controllers.SavePage("Hello World", "Integer posuere erat posuere velit aliquet.", nil)
	if err != nil {
		log.Fatalln(err)
	}
	_, err = controllers.SavePage("Hello2", "Aenean eu leo quam uam venenatis vestibulum.", nil)
	if err != nil {
		log.Fatalln(err)
	}
}

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {

	os.Remove("test.db")

	if err := db.SetupConnection(db.SQLITE, "test.db"); err != nil {
		log.Fatalf("Failed to create connection to database: %v", err)
	}

	if err := db.SetupSchema(); err != nil {
		log.Fatalf("Failed to initialize database schema: %v", err)
	}

	seed()

	e := echo.New()

	e.Static("/assets", "assets")

	e.GET("/", func(c echo.Context) error {

		t := &Template{
			templates: template.Must(template.ParseGlob("views/*.html")),
		}
		e.Renderer = t

		return c.Render(http.StatusOK, "index", map[string]interface{}{
			"Pages": controllers.GetAllPages(),
		})
	})

	e.GET("/page/:id", func(c echo.Context) error {

		t := &Template{
			templates: template.Must(template.ParseGlob("views/*.html")),
		}
		e.Renderer = t

		id := c.Param("id")
		i, err := strconv.Atoi(id)

		if err != nil {
			log.Warnln(err)
		}

		return c.Render(http.StatusOK, "page", map[string]interface{}{
			"Pages": controllers.GetAllPages(),
			"Page":  controllers.GetPageById(uint(i)),
		})

	})

	e.Logger.Fatal(e.Start(":1323"))
}
