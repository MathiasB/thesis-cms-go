package controllers

import (
	"github.com/DenBeke/cms/db"
	"github.com/DenBeke/cms/models"
	. "github.com/smartystreets/goconvey/convey"
	"log"
	"os"
	"testing"
)

func TestPagesController(t *testing.T) {
	Convey("Pages", t, func() {

		os.Remove("test.db")

		if err := db.SetupConnection(db.SQLITE, "test.db"); err != nil {
			log.Fatalf("Failed to create connection to database: %v", err)
		}

		if err := db.SetupSchema(); err != nil {
			log.Fatalf("Failed to initialize database schema: %v", err)
		}

		// Small seed
		page1, err := SavePage("Hello World", "Integer posuere erat posuere velit aliquet.", nil)
		So(err, ShouldEqual, nil)
		page2, err := SavePage("Hello2", "Aenean eu leo quam uam venenatis vestibulum.", nil)
		So(err, ShouldEqual, nil)

		Convey("SavePage", func() {

			// Make sure db is seeded
			pages := []*models.Page{}
			db.DB.Find(&pages)
			So(len(pages), ShouldEqual, 2)

			// Create user

		})

		Convey("GetAllPages", func() {

			pages := GetAllPages()
			So(len(pages), ShouldEqual, 2)
			So(pages[0].Title, ShouldEqual, page1.Title)
			So(pages[1].Title, ShouldEqual, page2.Title)

		})

		Convey("GetPageById", func() {

			page := GetPageById(page1.ID)
			So(page, ShouldNotEqual, nil)
			So(page.Title, ShouldEqual, page1.Title)

			// non-existing page
			page = GetPageById(42)
			So(page, ShouldEqual, nil)

		})

		Convey("Update", func() {

			page := GetPageById(page1.ID)
			So(page, ShouldNotEqual, nil)
			page.Title = "Test!"

			err := UpdatePage(page)
			So(err, ShouldEqual, nil)

			pageUser := GetPageById(page.ID)
			So(pageUser.Title, ShouldEqual, "Test!")

		})

		os.Remove("test.db")

	})
}
