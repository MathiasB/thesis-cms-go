package controllers

import (
	log "github.com/sirupsen/logrus"

	"github.com/DenBeke/cms/db"
	"github.com/DenBeke/cms/models"
	"github.com/jinzhu/gorm"
)

func SavePage(title string, content string, user *models.User) (page *models.Page, err error) {
	page = &models.Page{
		Title:   title,
		Content: content,
		User:    user,
	}

	if dbc := db.DB.Create(&page); dbc.Error != nil {
		err = dbc.Error
		return
	}

	return
}

func GetPageById(id uint) (page *models.Page) {
	page = &models.Page{}
	if dbc := db.DB.Where(&models.Page{Model: gorm.Model{ID: id}}).First(page); dbc.Error != nil {
		log.Warnln(dbc.Error)
		page = nil
	}
	return
}

func GetAllPages() (pages []*models.Page) {
	pages = []*models.Page{}
	if dbc := db.DB.Find(&pages); dbc.Error != nil {
		log.Warnln(dbc.Error)
	}
	return
}

func UpdatePage(page *models.Page) (err error) {
	if dbc := db.DB.Save(&page); dbc.Error != nil {
		err = dbc.Error
	}
	return
}
