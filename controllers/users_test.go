package controllers

import (
	"github.com/DenBeke/cms/db"
	"github.com/DenBeke/cms/models"
	. "github.com/smartystreets/goconvey/convey"
	"log"
	"os"
	"testing"
)

func TestUsersController(t *testing.T) {
	Convey("Users", t, func() {

		os.Remove("test.db")

		if err := db.SetupConnection(db.SQLITE, "test.db"); err != nil {
			log.Fatalf("Failed to create connection to database: %v", err)
		}

		if err := db.SetupSchema(); err != nil {
			log.Fatalf("Failed to initialize database schema: %v", err)
		}

		// Small seed
		alice, err := Register("Alice", "alice@test.com", "pwd")
		So(err, ShouldEqual, nil)
		bob, err := Register("Bob", "bob@test.com", "pwd2")
		So(err, ShouldEqual, nil)

		Convey("Register", func() {

			// Make sure db is empty
			users := []*models.User{}
			db.DB.Find(&users)
			So(len(users), ShouldEqual, 2)

			// Create user
			user, err := Register("John", "john@test.com", "pwd")
			So(err, ShouldEqual, nil)
			So(user.Name, ShouldEqual, "John")
			So(user.Email, ShouldEqual, "john@test.com")
			So(user.CheckPassword("pwd"), ShouldEqual, true)
			So(user.CheckPassword("grgrz"), ShouldEqual, false)

			db.DB.Find(&users)
			So(len(users), ShouldEqual, 3)

			// Try to create a second user with same email
			user, err = Register("John2", "john@test.com", "password")
			So(err, ShouldNotEqual, nil)
			So(len(users), ShouldEqual, 3)

			// Create second user with other email
			user, err = Register("John", "john2@test.com", "password")
			So(err, ShouldEqual, nil)
			So(user.Name, ShouldEqual, "John")
			So(user.Email, ShouldEqual, "john2@test.com")

		})

		Convey("GetAllUsers", func() {

			users := GetAllUsers()
			So(len(users), ShouldEqual, 2)
			So(users[0].Name, ShouldEqual, alice.Name)
			So(users[1].Name, ShouldEqual, bob.Name)

		})

		Convey("GetByEmail", func() {

			user := GetUserByEmail(alice.Email)
			So(user, ShouldNotEqual, nil)
			So(user.Name, ShouldEqual, alice.Name)

			// non-existing user
			user = GetUserByEmail("blablabla")
			So(user, ShouldEqual, nil)

		})

		Convey("GetById", func() {

			user := GetUserByID(bob.ID)
			So(user, ShouldNotEqual, nil)
			So(user.Name, ShouldEqual, bob.Name)

			// non-existing user
			user = GetUserByID(42)
			So(user, ShouldEqual, nil)

		})

		Convey("Update", func() {

			user := GetUserByID(bob.ID)
			So(user, ShouldNotEqual, nil)
			user.Name = "Craig"

			err := UpdateUser(user)
			So(err, ShouldEqual, nil)

			savedUser := GetUserByID(user.ID)
			So(savedUser.Name, ShouldEqual, "Craig")

		})

		os.Remove("test.db")

	})
}
