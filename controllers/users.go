package controllers

import (
	log "github.com/sirupsen/logrus"

	"github.com/DenBeke/cms/db"
	"github.com/DenBeke/cms/models"
	"github.com/jinzhu/gorm"
)

func Register(name string, email string, password string) (user *models.User, err error) {
	user, err = models.NewUser(name, email, password)
	if err != nil {
		return
	}

	if dbc := db.DB.Create(&user); dbc.Error != nil {
		err = dbc.Error
		return
	}

	return
}

func GetAllUsers() (users []*models.User) {
	users = []*models.User{}
	if dbc := db.DB.Find(&users); dbc.Error != nil {
		log.Warnln(dbc.Error)
	}
	return
}

func GetUserByEmail(email string) (user *models.User) {
	user = &models.User{}
	if dbc := db.DB.Where(&models.User{Email: email}).First(user); dbc.Error != nil {
		log.Warnln(dbc.Error)
		user = nil
	}
	return
}

func GetUserByID(id uint) (user *models.User) {
	user = &models.User{}
	if dbc := db.DB.Where(&models.User{Model: gorm.Model{ID: id}}).First(user); dbc.Error != nil {
		log.Warnln(dbc.Error)
		user = nil
	}
	return
}

func UpdateUser(user *models.User) (err error) {
	if dbc := db.DB.Save(&user); dbc.Error != nil {
		err = dbc.Error
	}
	return
}
