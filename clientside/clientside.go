package main


import (
    "net/http"
    "os"
    "strconv"

    "github.com/DenBeke/cms/controllers"
    "github.com/DenBeke/cms/db"

    log "github.com/sirupsen/logrus"
    "github.com/labstack/echo"
    "github.com/labstack/echo/middleware"
)



func seed() {
    _, err := controllers.SavePage("Hello World", "Integer posuere erat posuere velit aliquet.", nil)
    if err != nil {
        log.Fatalln(err)
    }
    _, err = controllers.SavePage("Hello2", "Aenean eu leo quam uam venenatis vestibulum.", nil)
    if err != nil {
        log.Fatalln(err)
    }
}




func main() {

    os.Remove("test.db")

    if err := db.SetupConnection(db.SQLITE, "test.db"); err != nil {
        log.Fatalf("Failed to create connection to database: %v", err)
    }

    if err := db.SetupSchema(); err != nil {
        log.Fatalf("Failed to initialize database schema: %v", err)
    }

    seed()



    e := echo.New()
    
    e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
    }))

    e.Static("/", "cms-vue/dist")
    
    
    e.GET("/pages", func(c echo.Context) error {
    
        pages := controllers.GetAllPages()
    
        return c.JSONPretty(http.StatusOK, pages, "    ")
    
    })


    e.GET("/page/:id", func(c echo.Context) error {

        id := c.Param("id")
        i, err := strconv.Atoi(id)

        if err != nil {
            log.Warnln(err)
        }
        
        page := controllers.GetPageById(uint(i))

        return c.JSONPretty(http.StatusOK, page, "    ")

    })


    e.Logger.Fatal(e.Start(":1323"))
}
