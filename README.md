CMS demo written in Go
----------------------

[![Build Status](https://travis-ci.com/DenBeke/cms.svg?token=55DZWEWREsf4wvhULGzt&branch=master)](https://travis-ci.com/DenBeke/cms)

Very, very, very simple CMS written in Go, for master thesis project.  
The CMS has a client-side rendered version and a server-side rendered version.

### Server-side rendered version

Server-side rendered version can be found in the `server-side` folder and can be ran by building the executable and then running it:

    $ go build
    $ ./serverside
    
This serves the CMS at `localhost:1323`.



### Client-side rendered version

Client-side rendered version can be found in the `clientside` folder.  
In order to run it first the REST API should be built and ran:

    $ go build
    $ ./clientside
   
Then the front-end app must be installed and ran. Go to the `clientside/cms-vue` folder and run:

    $ npm install
    $ npm run dev

This serves the front-end app at `localhost:8080` which access the running REST API at `localhost:1323`.



### Author

[Mathias Beke](https://denbeke.be)